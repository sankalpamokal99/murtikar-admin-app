import 'package:flutter/material.dart';
import 'package:murtikar_admin_app/provider/products_provider.dart';
import 'package:murtikar_admin_app/screens/admin.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: ProductProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Admin(),
      )));
}
